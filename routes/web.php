<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', 'DashboardController@index')->name('home');

/* member registration form */
Route::get('/signup', 'Auth\SignUpController@index')->name('signup');
Route::post('/signup', 'Auth\SignUpController@store');
Route::get('/signup_done', 'Auth\SignUpController@signup_done');
Route::get('/verify', 'Auth\SignUpController@verify')->name('verify');

/* Login section */
Route::get('/login', 'Auth\CustomLoginController@index')->name('login');
Route::get('/403', 'Auth\CustomLoginController@not_allowed')->name('unauthorized');
Route::post('/login', 'Auth\CustomLoginController@login_proses');

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth'])->group(function () {

Route::prefix('acl')->group(function () {

  Route::prefix('role')->group(function () {
    Route::get('/', 'ACL\RoleController@index');
    Route::get('/add', 'ACL\RoleController@create');
    Route::get('/edit/{id}', 'ACL\RoleController@edit');
    Route::get('/delete/{id}', 'ACL\RoleController@destroy');
    Route::get('/datatables', 'ACL\RoleController@list_datatables_api');
    /* Post section */
    Route::post('/add', 'ACL\RoleController@store');
    Route::post('/edit/{id}', 'ACL\RoleController@update');
  });

  Route::prefix('menu')->group(function () {
    Route::get('/', 'ACL\MenuController@index');
    Route::get('/add', 'ACL\MenuController@create');
    Route::get('/edit/{id}', 'ACL\MenuController@edit');
    Route::get('/delete/{id}', 'ACL\MenuController@destroy');
    Route::get('/datatables', 'ACL\MenuController@list_datatables_api');
    /* Post section */
    Route::post('/add', 'ACL\MenuController@store');
    Route::post('/edit/{id}', 'ACL\MenuController@update');
  });

  Route::prefix('permission')->group(function () {
    Route::get('/{role?}', 'ACL\PermissionController@index');
    Route::post('/{role?}', 'ACL\PermissionController@store');
  });

  Route::prefix('user')->group(function () {
    Route::get('/', 'ACL\UserController@index');
    Route::get('/add', 'ACL\UserController@create');
    Route::get('/edit/{id}', 'ACL\UserController@edit');
    Route::get('/delete/{id}', 'ACL\UserController@destroy');
    Route::get('/datatables', 'ACL\UserController@list_datatables_api');
    /* Post section */
    Route::post('/add', 'ACL\UserController@store');
    Route::post('/edit/{id}', 'ACL\UserController@update');
  });

});

Route::prefix('mst')->group(function () {
    Route::group(['prefix'=>'submission', 'middleware'=>'MenuAccess'], function () {
    Route::get('/', 'Mst\SubmissionController@index');
    Route::get('/add', 'Mst\SubmissionController@create');
    Route::get('/edit/{guid}', 'Mst\SubmissionController@edit');
    Route::get('/delete/{guid}', 'Mst\SubmissionController@destroy');
    Route::get('/datatables', 'Mst\SubmissionController@list_datatables_api');
    Route::get('/dl/{filename}', 'Mst\SubmissionController@download_file');
    Route::get('/update_submission_status/{status}/{guid}', 'Mst\SubmissionController@update_submission_status');
    /* Post section */
    Route::post('/add', 'Mst\SubmissionController@store');
    Route::post('/edit/{id}', 'Mst\SubmissionController@update');
  });
});


});
