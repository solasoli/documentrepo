<?php

use Illuminate\Database\Seeder;

class DownloadOptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('mst_download_option')->insert([
        [
          'id' => 1,
          'name' => 'CIFOR allow public to download from CIFOR site'
        ]
      ]);
      DB::table('mst_download_option')->insert([
        [
          'id' => 2,
          'name' => 'Download is granted for Reviewer only'
        ]
      ]);
    }
}
