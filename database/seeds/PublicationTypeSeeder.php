<?php

use Illuminate\Database\Seeder;

class PublicationTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('mst_publication_type')->insert([
        [
          'id' => 1,
          'name' => 'Journal article'
        ]
      ]);
      DB::table('mst_publication_type')->insert([
        [
          'id' => 2,
          'name' => 'In-house publication'
        ]
      ]);
    }
}
