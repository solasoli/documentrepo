<?php

use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      //
      DB::table('acl_permission')->insert([
        [
          'id' => 1,
          'id_menu' => 1,
          'id_role' => 1,
          'view' => 1,
          'add' => 1,
          'edit' => 1,
          'delete' => 1,
          'approve' => 1,
          'reject' => 1,
          'suspend' => 1
        ]
      ]);
      DB::table('acl_permission')->insert([
        [
          'id' => 2,
          'id_menu' => 1,
          'id_role' => 2,
          'view' => 1,
          'add' => 1,
          'edit' => 1,
          'delete' => 1,
          'approve' => 1,
          'reject' => 1,
          'suspend' => 1
        ]
      ]);

      //
      DB::table('acl_permission')->insert([
        [
          'id' => 3,
          'id_menu' => 2,
          'id_role' => 1,
          'view' => 1,
          'add' => 1,
          'edit' => 1,
          'delete' => 1,
          'approve' => 1,
          'reject' => 1,
          'suspend' => 0
        ]
      ]);
      DB::table('acl_permission')->insert([
        [
          'id' => 4,
          'id_menu' => 2,
          'id_role' => 2,
          'view' => 1,
          'add' => 1,
          'edit' => 1,
          'delete' => 1,
          'approve' => 1,
          'reject' => 1,
          'suspend' => 0
        ]
      ]);
      DB::table('acl_permission')->insert([
        [
          'id' => 5,
          'id_menu' => 2,
          'id_role' => 3,
          'view' => 1,
          'add' => 1,
          'edit' => 1,
          'delete' => 0,
          'approve' => 0,
          'reject' => 0,
          'suspend' => 0
        ]
      ]);
    }
}
