<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('acl_role')->insert([
        [
          'nama' => "Administrator"
        ]
      ]);
      DB::table('acl_role')->insert([
        [
          'nama' => "Reviewer"
        ]
      ]);
      DB::table('acl_role')->insert([
        [
          'nama' => "Member"
        ]
      ]);
    }
}
