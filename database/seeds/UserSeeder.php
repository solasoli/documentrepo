<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([
        [
          'id' => 1,
          'guid' => Str::uuid(),
          'username' => 'admin',
          'email' => "administrator@localhost.com",
          'password' => Hash::make('12345'),
          'id_role' => 1,
          'email_verified_at' => date('Y-m-d H:i:s')
        ]
      ]);
      DB::table('users')->insert([
        [
          'id' => 2,
          'guid' => Str::uuid(),
          'username' => 'reviewer',
          'email' => "reviewer@localhost.com",
          'password' => Hash::make('12345'),
          'id_role' => 2,
          'email_verified_at' => date('Y-m-d H:i:s')
        ]
      ]);
      DB::table('users')->insert([
        [
          'id' => 3,
          'guid' => Str::uuid(),
          'username' => 'member',
          'email' => "member@localhost.com",
          'password' => Hash::make('12345'),
          'id_role' => 3,
          'email_verified_at' => date('Y-m-d H:i:s')
        ]
      ]);
    }
}
