<?php

use Illuminate\Database\Seeder;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('acl_menu')->insert([
        [
          'id' => 1,
          'nama' => 'User',
          'url' => '/acl/user',
          'slug' => 'user',
          'created_by' => 1
        ]
      ]);
      DB::table('acl_menu')->insert([
        [
          'id' => 2,
          'nama' => 'Submission',
          'url' => '/mst/submission',
          'slug' => 'submission',
          'created_by' => 1
        ]
      ]);
    }
}
