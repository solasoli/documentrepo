<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIsSuspended extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('users', function (Blueprint $table) {
        $table->dateTime('suspended_at')->nullable();
        $table->integer('suspended_by')->default(0);
        $table->boolean('is_suspended')->default(0);

        $table->dropColumn('id_role');
      });

      Schema::table('users', function (Blueprint $table) {
        $table->integer('id_role')->after('id');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user', function (Blueprint $table) {
            //
        });
    }
}
