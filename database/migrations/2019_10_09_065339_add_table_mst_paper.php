<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableMstPaper extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_paper', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('judul');
            $table->string('author');
            $table->string('penulis');
            $table->string('tahun_terbit', '4');
            $table->string('link_berita')->nullable();
            $table->string('file')->nullable();
            $table->tinyInteger('id_status_approval')->default(0);

            $table->dateTime('created_at')->nullable();
            $table->integer('created_by')->default(0);
            $table->dateTime('updated_at')->nullable();
            $table->integer('updated_by')->default(0);
            $table->dateTime('deleted_at')->nullable();
            $table->integer('deleted_by')->default(0);
            $table->boolean('is_deleted')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_paper');
    }
}
