<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnLogApprovedRejectedMstSubmission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mst_submission', function (Blueprint $table) {
          $table->dateTime('approved_at')->after('status_id')->nullable();
          $table->integer('approved_by')->after('approved_at')->default(0);
          $table->dateTime('rejected_at')->after('approved_by')->nullable();
          $table->integer('rejected_by')->after('rejected_at')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mst_submission', function (Blueprint $table) {
            //
        });
    }
}
