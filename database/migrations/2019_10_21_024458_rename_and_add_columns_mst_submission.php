<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameAndAddColumnsMstSubmission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mst_submission', function (Blueprint $table) {
          // drop columns first
          $table->dropColumn('judul');
          $table->dropColumn('author');
          $table->dropColumn('penulis');
          $table->dropColumn('tahun_terbit');
          $table->dropColumn('link_berita');
          $table->dropColumn('file');
          $table->dropColumn('id_status_approval');

          // add columns
          $table->string('title');
          $table->string('sub_title');
          $table->string('author_firstname');
          $table->string('author_middlename');
          $table->string('author_lastname');
          $table->integer('publication_type_id');
          $table->string('abstract');
          $table->integer('research_site_country');
          $table->string('research_site_region');
          $table->string('source_link')->nullable();
          $table->string('soft_copy')->nullable();
          $table->integer('download_option_id')->default(0);
          $table->integer('status_id');
          $table->integer('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mst_submission', function (Blueprint $table) {
            //
        });
    }
}
