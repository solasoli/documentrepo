<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsInUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
          $table->string('title')->after('password')->nullable();
          $table->string('affiliation')->after('title')->nullable();
          $table->string('organization')->after('affiliation')->nullable();
          $table->integer('country_id')->after('organization')->default(0);

          $table->integer('created_by')->after('created_at')->default(0);
          $table->integer('updated_by')->after('updated_at')->default(0);

          $table->dateTime('deleted_at')->after('updated_by')->nullable();
          $table->integer('deleted_by')->after('deleted_at')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
