<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableMstStatusApproval extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('mst_status_approval', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->string('nama');
      });

      /* ini baku */
      DB::table('mst_status_approval')->insert([
        [
          'id' => 1,
          'nama' => "Pending"
        ]
      ]);

      DB::table('mst_status_approval')->insert([
        [
          'id' => 2,
          'nama' => "Approved"
        ]
      ]);

      DB::table('mst_status_approval')->insert([
        [
          'id' => 3,
          'nama' => "Rejected"
        ]
      ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_status_approval');
    }
}
