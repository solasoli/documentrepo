<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnsPositionMstSubmission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mst_submission', function (Blueprint $table) {
          // drop columns first
          $table->dropColumn('created_at');
          $table->dropColumn('created_by');
          $table->dropColumn('updated_at');
          $table->dropColumn('updated_by');
          $table->dropColumn('deleted_at');
          $table->dropColumn('deleted_by');
          $table->dropColumn('is_deleted');
        });

        Schema::table('mst_submission', function (Blueprint $table) {
          // add columns
          $table->dateTime('created_at')->nullable();
          $table->integer('created_by')->default(0);
          $table->dateTime('updated_at')->nullable();
          $table->integer('updated_by')->default(0);
          $table->dateTime('deleted_at')->nullable();
          $table->integer('deleted_by')->default(0);
          $table->boolean('is_deleted')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mst_submission', function (Blueprint $table) {
            //
        });
    }
}
