<?php

namespace App\Http\Controllers\ACL;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Datatables;
use Validator;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Input;
use App\User;
use App\Role;
use Hash;

date_default_timezone_set('Asia/Jakarta');

class UserController extends Controller
{
    public function index()
    {
      return view('acl.user-list');
    }

    public function create()
    {
      $role = Role::where('is_deleted', 0);
      if(Auth::user()->id_role == 1){
        $role = $role->whereIn('id', [2,3]);
      }
      else {
        $role = $role->where('id', 3);
      }

      $role = $role->get();


      return view('acl.user-form', [
        'role' => $role
      ]);
    }

    public function store(Request $request)
    {
      $logged_user = Auth::user();
      request()->validate([
        'username' => [
          'required',
          Rule::unique('users', 'username')->where(function ($query){
            return $query->where('is_deleted', 0);
          })
        ],
        'nama' => 'required',
        'role' => 'required',
        'email' => [
          'required',
          Rule::unique('users', 'email')->where(function ($query){
            return $query->where('is_deleted', 0);
          }),
          'email'
        ],
        'password' => 'required',
        'conf_password' => 'required|same:password'
      ]);

      $t = new User;
      $t->name = $request->input('nama');
      $t->id_role = $request->input('role');
      $t->email = $request->input('email');
      $t->username = $request->input('username');
      $t->password = Hash::make($request->input('password'));
      $t->is_deleted = 0;
      $t->save();

      $request->session()->flash('message', "<strong>".$request->input('nama')."</strong> Berhasil disimpan!");
      return redirect('/acl/user');
    }

    public function edit($id)
    {
      $data = User::findOrFail($id);

      $role = Role::where('is_deleted', 0);
      if(Auth::user()->id_role == 1){
        $role = $role->whereIn('id', [2,3,4]);
      }
      else {
        $role = $role->whereIn('id', [3,4]);
      }

      $role = $role->get();

      return view('acl.user-form', [
        'data' => $data,
        'role' => $role
      ]);
    }

    public function update(Request $request, $id)
    {
      $logged_user = Auth::user();
      request()->validate([
        'username' => [
          'required',
          Rule::unique('users', 'username')->where(function ($query) use ($id){
            return $query->where('is_deleted', 0)->where("id", "!=", $id);
          })
        ],
        'nama' => 'required',
        'role' => 'required',
        'email' => [
          'required',
          Rule::unique('users', 'email')->where(function ($query)use ($id){
            return $query->where('is_deleted', 0)->where("id", "!=", $id);
          }),
          'email'
        ]
      ]);

      if($request->input('password') != ''){
        request()->validate([
          'password' => 'required',
          'conf_password' => 'required|same:password'
        ]);
      }

      $t = User::findOrFail($id);
      $t->name = $request->input('nama');
      $t->id_role = $request->input('role');
      $t->email = $request->input('email');
      $t->username = $request->input('username');
      if($request->input('password') != ''){
        $t->password = Hash::make($request->input('password'));
      }
      $t->is_deleted = 0;
      $t->save();

      $request->session()->flash('message', "Data berhasil diubah!");
      return redirect('/acl/user');
    }

    public function destroy(Request $request, $id)
    {
      $logged_user = Auth::user();
      $t = User::findOrFail($id);
      $t->deleted_at = date('Y-m-d H:i:s');
      $t->deleted_by = Auth::id();
      $t->is_deleted = 1;
      $t->save();

      $request->session()->flash('message', "<strong>".$t->nama."</strong> berhasil Dihapus!");
      return redirect('/acl/user');
    }

    public function list_datatables_api()
    {
      $data = DB::table("users AS u")
      ->select(DB::raw("u.id_role, u.guid, u.username, u.email, u.email_verified_at, u.title, u.affiliation, u.organization, c.name AS country, is_suspended,
      r.nama AS role"))
      ->leftjoin('mst_country AS c', 'c.id', '=', 'u.country_id')
      ->join("acl_role AS r", "u.id_role" , "=" , "r.id");

      if(Auth::user()->id_role == 1){ // jika admin, maka bisa CRUD reviewer & member
        $data = $data->whereIn('r.id', [2,3]);
      }
      else { // else, bisa CRUD member
        $data = $data->where('r.id', 3);
      }

      $data = $data->orderBy('u.id', 'ASC')->get();

      return Datatables::of($data)->make(true);
    }
}
