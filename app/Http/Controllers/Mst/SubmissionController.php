<?php

namespace App\Http\Controllers\Mst;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Datatables;
use Validator;
use Auth;
use Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Mst\Submission;
use App\Mst\SubmissionStatus;
use App\Mst\Country;
use App\Mst\DownloadOption;
use App\Mst\PublicationType;
use App\User;

date_default_timezone_set('Asia/Jakarta');

class SubmissionController extends Controller
{
    public function submission_id($guid)
    {
      $submission_id = Submission::where('is_deleted', 0)->where('guid', $guid)->get()->first();
      $submission_id = isset($submission_id->id) ? $submission_id->id : 0;
      return $submission_id;
    }

    public function index()
    {
      $submission_status = SubmissionStatus::orderBy('id', 'ASC')->get();

      return view('mst.submission-list',[
        'submission_status' => $submission_status,
      ]);
    }

    public function create()
    {
      $download_option = DownloadOption::orderBy('id', 'ASC')->get();
      $country = Country::orderBy('id', 'ASC')->get();
      $publication_type = PublicationType::orderBy('id', 'ASC')->get();

      return view('mst.submission-form',[
        'download_option' => $download_option,
        'country' => $country,
        'publication_type' => $publication_type,
      ]);
    }

    public function store(Request $request)
    {
      $logged_user = Auth::user();

      $validator = Validator::make($request->all(), [
        'title' => 'required',
        'sub_title' => 'required',
        'author_firstname' => 'required',
        'author_middlename' => 'required',
        'author_lastname' => 'required',
        'publication_type' => 'required',
        'abstract' => 'required',
        'research_site_country' => 'required',
        'research_site_region' => 'required',
        'source_link' => 'required_without:soft_copy',
        'soft_copy' => 'required_without:source_link',
      ],[
        'title.required' => 'The title field is required.',
        'sub_title.required' => 'The Sub title field is required.',
        'author_firstname.required' => 'The Author first name field is required.',
        'author_middlename.required' => 'The Author middle name field is required.',
        'author_lastname.required' => 'The Author last name field is required.',
        'publication_type.required' => 'The Publication type field is required.',
        'abstract.required' => 'The Abstract field is required.',
        'research_site_country.required' => 'The Research site country field is required.',
        'research_site_region.required' => 'The Research site region field is required.',
      ]);

      if($validator->fails()){
        return back()
              ->withErrors($validator)
              ->withInput();
      }
      //die();

      $uploadedFile = $request->file('soft_copy');
      $filename = '';
      if(isset($uploadedFile)){
        $filename = $uploadedFile->store('public/file');
        $filename = str_replace("public/file/", "", $filename); // Outputnya "randomname.pdf"
      }

      $t = new Submission;
      $t->guid = Str::uuid();
      $t->title = $request->input('title');
      $t->sub_title = $request->input('sub_title');
      $t->author_firstname = $request->input('author_firstname');
      $t->author_middlename = $request->input('author_middlename');
      $t->author_lastname = $request->input('author_lastname');
      $t->publication_type_id = $request->input('publication_type');
      $t->abstract = $request->input('abstract');
      $t->research_site_country = $request->input('research_site_country');
      $t->research_site_region = $request->input('research_site_region');
      $t->source_link = $request->input('source_link');
      $t->soft_copy = $filename;
      $t->download_option_id = $request->input('copyright_clearance') != '' ? $request->input('copyright_clearance') : 0;
      $t->status_id = 1; // 1 = pending
      $t->user_id = Auth::id();
      $t->created_at = date("Y-m-d H:i:s");
      $t->created_by = Auth::id();
      $t->save();

      $request->session()->flash('message', "Data saved successfully");
      return redirect('/mst/submission');
    }

    public function edit($guid)
    {
      $submission_id = $this->submission_id($guid);

      $data = Submission::findOrFail($submission_id);

      $download_option = DownloadOption::orderBy('id', 'ASC')->get();
      $country = Country::orderBy('id', 'ASC')->get();
      $publication_type = PublicationType::orderBy('id', 'ASC')->get();

      return view('mst.submission-form',[
        'data' => $data,
        'download_option' => $download_option,
        'country' => $country,
        'publication_type' => $publication_type,
      ]);
    }

    public function update(Request $request, $guid)
    {
      $logged_user = Auth::user();

      $validator = Validator::make($request->all(), [
        'title' => 'required',
        'sub_title' => 'required',
        'author_firstname' => 'required',
        'author_middlename' => 'required',
        'author_lastname' => 'required',
        'publication_type' => 'required',
        'abstract' => 'required',
        'research_site_country' => 'required',
        'research_site_region' => 'required',
        // 'source_link' => 'required_without:soft_copy',
        // 'soft_copy' => 'required_without:source_link',
      ],[
        'title.required' => 'The title field is required.',
        'sub_title.required' => 'The Sub title field is required.',
        'author_firstname.required' => 'The Author first name field is required.',
        'author_middlename.required' => 'The Author middle name field is required.',
        'author_lastname.required' => 'The Author last name field is required.',
        'publication_type.required' => 'The Publication type field is required.',
        'abstract.required' => 'The Abstract field is required.',
        'research_site_country.required' => 'The Research site country field is required.',
        'research_site_region.required' => 'The Research site region field is required.',
      ]);

      if($validator->fails()){
        return back()
              ->withErrors($validator)
              ->withInput();
      }
      //die();

      $submission_id = $this->submission_id($guid);

      if($request->file('soft_copy') == ""){
        $filename = Submission::findOrFail($submission_id)->first()->soft_copy;
      }
      else{
        $uploadedFile = $request->file('soft_copy');
        $filename = $uploadedFile->store('public/file');
        $filename = str_replace("public/file/", "", $filename); // Outputnya "randomname.pdf"
      }

      //echo $filename; die();

      $t = Submission::findOrFail($submission_id);
      $t->title = $request->input('title');
      $t->sub_title = $request->input('sub_title');
      $t->author_firstname = $request->input('author_firstname');
      $t->author_middlename = $request->input('author_middlename');
      $t->author_lastname = $request->input('author_lastname');
      $t->publication_type_id = $request->input('publication_type');
      $t->abstract = $request->input('abstract');
      $t->research_site_country = $request->input('research_site_country');
      $t->research_site_region = $request->input('research_site_region');
      $t->source_link = $request->input('source_link');
      $t->soft_copy = $filename;
      $t->download_option_id = $request->input('copyright_clearance') != '' ? $request->input('copyright_clearance') : 0;
      $t->updated_at = date("Y-m-d H:i:s");
      $t->updated_by = Auth::id();
      $t->save();

      $request->session()->flash('message', "Data successfully changed");
      return redirect("/mst/submission/edit/{$guid}");
    }

    public function destroy(Request $request, $guid)
    {
      $logged_user = Auth::user();

      $submission_id = $this->submission_id($guid);

      $t = Submission::findOrFail($submission_id);
      $t->deleted_at = date("Y-m-d H:i:s");
      $t->deleted_by = Auth::id();
      $t->is_deleted = 1;
      $t->save();

      $request->session()->flash('message', "Data successfully deleted");
      return redirect('/mst/submission');
    }

    public function list_datatables_api()
    {
      $data = DB::table("mst_submission AS s")
      ->select(DB::raw('s.*, pt.name AS publication_type, c.name AS country, ss.name AS status'))
      ->join('mst_submission_status AS ss', 'ss.id', '=', 's.status_id')
      ->join('mst_publication_type AS pt', 'pt.id', '=', 's.publication_type_id')
      ->join('mst_country AS c', 'c.id', '=', 's.research_site_country')
      ->where('s.is_deleted', 0);

      if(Auth::user()->id_role == 3){ // 3 = member
        $data = $data->where('s.user_id', Auth::user()->id);
      }

      $data = $data->orderBy("s.title", "ASC")->get();

      return Datatables::of($data)->make(true);
    }

    public function download_file($guid)
    {
      $submission_id = $this->submission_id($guid);

      $t = Submission::findOrFail($submission_id);

      $path = "public/file/".$t->soft_copy;
      return Storage::download($path);
    }

    public function update_submission_status(Request $request, $status, $guid){
      $submission_id = $this->submission_id($guid);

      $t = Submission::findOrFail($submission_id);
      $t->status_id = $status;

      if($status == 2){ // approved
        $t->approved_at = date("Y-m-d H:i:s");
        $t->approved_by = Auth::id();
      }

      if($status == 3){ // rejected
        $t->rejected_at = date("Y-m-d H:i:s");
        $t->rejected_by = Auth::id();
      }

      $t->save();

      $msg = $status == 2 ? "Data successfully approved" : $status == 3 ? "Data successfully rejected" : "";
      $request->session()->flash('message', $msg);
      return redirect('/mst/submission');
    }
}
