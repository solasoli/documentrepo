<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Mst\Country;
use Validator;
use Mail;
use Crypt;
use Str;
use Session;
use App\Mail\VerifyEmail;
use App\Mail\NewMemberNotification;

class SignUpController extends Controller
{
    public function __construct()
    {
      $this->middleware('csrf');
    }

    public function index(Request $request)
    {
      $country = Country::orderBy('id', 'ASC')->get();
      return view('auth.signup-form', [
        'country' => $country
      ]);
    }

    public function store(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'username' => 'required|unique:users',
        'email' => 'required|email|unique:users',
        'password' => 'required|confirmed|min:4',
        'title' => 'required',
        'affiliation' => 'required',
        'organization' => 'required',
        'country' => 'required',
      ]);

      if($validator->fails()){
        return back()
              ->withErrors($validator)
              ->withInput();
      }


      DB::transaction(function () use ($request){
        // save into table
        $t = new User;
        $t->guid = Str::uuid();
        $t->username = $request->input('username');
        $t->email = $request->input('email');
        $t->password = bcrypt($request->input('password'));
        $t->title = $request->input('title');
        $t->affiliation = $request->input('affiliation');
        $t->organization = $request->input('organization');
        $t->country_id = $request->input('country');
        $t->created_at = date("Y-m-d H:i:s");
        $t->id_role = 3; // member
        $t->save();

        // send email verification
        Mail::to($t->email)->send(new VerifyEmail($t));
      });

      return redirect('/signup_done');
    }

    public function signup_done()
    {
      return view('auth.signup_done');
    }

    public function verify()
    {
      // if token is not provided
      if(empty(request('token'))){
        return redirect()->route('signup');
      }

      // decrypt token as email
      $decryptedEmail = Crypt::decrypt(request('token'));

      // find user by email
      $user = User::where('email', $decryptedEmail)->get()->first();

      // if user is already verify
      if($user->email_verified_at != ''){
        Session::flash('warning', 'Your email address has already been verified.');
        return redirect('/login');
      }

      DB::transaction(function () use ($user){
        // verify user by updating column email_verified_at
        $user->email_verified_at = date("Y-m-d H:i:s");
        $user->save();

        // send an email to the admin and reviewer that a new member has joined
        $admin_reviewer = User::where('is_deleted', 0)->where('is_suspended', 0)->whereIn('id_role', [1,2])->get();
        foreach($admin_reviewer as $idx => $row){
          Mail::to($row->email)->send(new NewMemberNotification($user));
        }
      });

      //
      Session::flash('success', 'Your email address has been successfully verified.');
      return redirect('/login');
    }
}
