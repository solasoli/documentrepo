<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\User as User;
use Session;
class CustomLoginController extends Controller
{

    public function __construct(){
        $this->middleware('csrf');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('auth.login');
    }

    public function login_proses(Request $request){

        /* find user */
        $find_user = User::where('username', $request->input('username'))->first();
        if(Auth::attempt([
            'username' => $request->input('username'),
            'password' =>$request->input('password')
        ])){
          // if the user hasn't verified
          if($find_user->email_verified_at == '' || $find_user->email_verified_at == null){
            Session::flash('danger', 'Your email address has not been verified.');
            return redirect('/login');
          }

          // if the user is suspended
          if($find_user->is_suspended > 0){
            Session::flash('danger', 'Your account has been suspended.');
            return redirect('/login');
          }

          // if the user is deleted
          if($find_user->is_deleted > 0){
            Session::flash('danger', 'Your account has been deleted.');
            return redirect('/login');
          }

          Auth::user()->push('name', $find_user->name);
          Auth::user()->push('role', $find_user->roles);
          return redirect('/');
        } else {
          /* balikin ke login */
          Session::flash('danger', 'You have entered an invalid username or password.');
          return redirect('/login');
        }

    }



    function not_allowed(){
        return view('page_403');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
