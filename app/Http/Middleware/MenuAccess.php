<?php

namespace App\Http\Middleware;

use Closure;
use App\Menu;

class MenuAccess
{
    public function handle($request, Closure $next)
    {
      $slug = $request->segment(2);

      if(!can_access($slug, "view")){
        return redirect("/");
      }

      return $next($request);
    }
}
