<?php

namespace App\Mst;

use Illuminate\Database\Eloquent\Model;

class DownloadOption extends Model
{
  protected $table = "mst_download_option";
  protected $primaryKey = "id";
  public $timestamps = false;
}
