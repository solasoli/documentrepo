<?php

namespace App\Mst;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
  protected $table = "mst_country";
  protected $primaryKey = "id";
  public $timestamps = false;
}
