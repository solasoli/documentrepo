<?php

namespace App\Mst;

use Illuminate\Database\Eloquent\Model;

class PublicationType extends Model
{
  protected $table = "mst_publication_type";
  protected $primaryKey = "id";
  public $timestamps = false;
}
