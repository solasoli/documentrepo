<?php

namespace App\Mst;

use Illuminate\Database\Eloquent\Model;

class SubmissionStatus extends Model
{
  protected $table = "mst_submission_status";
  protected $primaryKey = "id";
  public $timestamps = false;
}
