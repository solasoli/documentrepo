<?php

namespace App\Mst;

use Illuminate\Database\Eloquent\Model;

class Submission extends Model
{
  protected $table = "mst_submission";
  protected $primaryKey = "id";
  public $timestamps = false;
}
