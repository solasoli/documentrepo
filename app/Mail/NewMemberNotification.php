<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;
use App\Mst\Country;

class NewMemberNotification extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
      $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      $country = Country::findOrFail($this->user->country_id);

      return $this->subject('New member just joined')
        ->view('email.msg_new_member_notif', [
          'username' => $this->user->username,
          'email' => $this->user->email,
          'title' => $this->user->title,
          'affiliation' => $this->user->affiliation,
          'organization' => $this->user->organization,
          'country' => $country->name,
        ]);
    }
}
