<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Crypt;
use App\User;

class VerifyEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
      $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      // generate link
      $encryptedEmail = Crypt::encrypt($this->user->email);

      // ex: domain.com/verify?token=xxxx
      $link = route('verify', ['token' => $encryptedEmail]);
      return $this->subject('Verify Your Email Address')
        // ->with('link', $link)
        ->view('email.msg_signup_verification', [
          'link' => $link,
          'username' => $this->user->username
        ]);
    }
}
