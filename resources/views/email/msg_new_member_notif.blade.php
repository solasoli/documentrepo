<p>New member just joined.</p>
<br>
<table border="0">
  <tr>
    <td>Username</td>
    <td>: {{$username}}</td>
  </tr>
  <tr>
    <td>Email</td>
    <td>: {{$email}}</td>
  </tr>
  <tr>
    <td>Title</td>
    <td>: {{$title}}</td>
  </tr>
  <tr>
    <td>Affiliation</td>
    <td>: {{$affiliation}}</td>
  </tr>
  <tr>
    <td>Organization</td>
    <td>: {{$organization}}</td>
  </tr>
  <tr>
    <td>Country</td>
    <td>: {{$country}}</td>
  </tr>
</table>
