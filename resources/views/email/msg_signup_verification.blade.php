<p>Hi <b>{{$username}}</b>,</p>
<p>Please click link below to verify your email address.</p>
<p><a href="{{$link}}">{{$link}}</a></p>
<p>Thank you.</p>
