@extends('layouts.app')

@section('content')
@if(Session::has('message'))
  <p class="alert alert-success">{!! Session::get('message') !!}</p>
@endif
<div class="containter">
  <h3>Dashboard</h3>
  <div class="row top_tiles">
</div>

<script>

$(function() {
  setTimeout(function() {
    $(".alert-success").hide(1000);
  }, 5000);
});
</script>
@endsection
