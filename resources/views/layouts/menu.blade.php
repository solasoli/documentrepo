<li>
  <a href='/'>
    <i class="fa fa-dashboard"></i> Dashboard
  </a>
</li>

<li><a><i class="fa fa-book"></i> Master<span class="fa fa-chevron-down"></span></a>
  <ul class="nav child_menu">
    @if(can_access('submission', 'view'))
    <li><a href="/mst/submission">Submission</a></li>
    @endif
  </ul>
</li>

@if(can_access('user', 'view'))
<li>
  <a href='/acl/user'>
    <i class="fa fa-user"></i> User
  </a>
</li>
@endif

@if(Auth::user()->id_role == 1)
<li><a><i class="fa fa-book"></i> System<span class="fa fa-chevron-down"></span></a>
  <ul class="nav child_menu">
    <li><a href="/acl/role">Role</a></li>
    <li><a href="/acl/menu">Menu</a></li>
    <li><a href="/acl/permission">Permission</a></li>
    <!-- <li><a href="/acl/user">User</a></li> -->
  </ul>
</li>
@endif
