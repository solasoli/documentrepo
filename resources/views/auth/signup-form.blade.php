<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Apps | Sign Up</title>

  <!-- Bootstrap -->
  <link href="{{ asset('vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
  <!-- Font Awesome -->
  <link href="{{ asset('vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
  <!-- NProgress -->
  <link href="{{ asset('vendors/nprogress/nprogress.css') }}" rel="stylesheet">
  <!-- Animate.css -->
  <link href="{{ asset('vendors/animate.css/animate.min.css') }}" rel="stylesheet">
  <!-- Custom Theme Style -->
  <link href="{{ asset('build/css/custom.min.css') }}" rel="stylesheet">
  <!-- jQuery -->
  <script src="{{ asset('/vendors/jquery/dist/jquery.min.js') }}"></script>
</head>

<body class="login">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
          <div class="panel-heading"><h4 style="color:#73879c">Sign Up</h4></div>

          <div class="panel-body">
            @if ($errors->any())
              <div class="alert alert-danger">
                <ul class="list-unstyled">
                  @foreach ($errors->all() as $error)
                    <li>&#8226; {{ $error }}</li>
                  @endforeach
                </ul>
              </div>
            @endif
            <form class="form-horizontal" method="POST" action="{{ route('signup') }}">
              {{ csrf_field() }}

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">
                  Username <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input id="username" name='username' value='{{ !is_null(old('username')) ? old('username') : '' }}'  class="form-control col-md-7 col-xs-12" type="text">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">
                  E-mail <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input id="email" name='email' value='{{ !is_null(old('email')) ? old('email') : '' }}'  class="form-control col-md-7 col-xs-12" type="text">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">
                  Password <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input id="password" name='password'  class="form-control col-md-7 col-xs-12" type="password">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">
                  Password confirmation <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input id="password_confirmation" name='password_confirmation'  class="form-control col-md-7 col-xs-12" type="password">
                  <span class="text-warning" id='divCheckPasswordMatch'></span>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">
                  Title <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input name='title' value='{{ !is_null(old('title')) ? old('title') : '' }}'  class="form-control col-md-7 col-xs-12" type="text">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">
                  Affiliation <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input name='affiliation' value='{{ !is_null(old('affiliation')) ? old('affiliation') : '' }}'  class="form-control col-md-7 col-xs-12" type="text">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">
                  Organization <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input name='organization' value='{{ !is_null(old('organization')) ? old('organization') : '' }}'  class="form-control col-md-7 col-xs-12" type="text">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" >
                  Country<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <select class="form-control" name="country">
                    <option value="">- Choose -</option>
                    @foreach($country AS $row)
                    @php
                    $selected = !is_null(old('country')) ? old('country') : '';
                    $selected = $selected == $row->id ? "selected" : "";
                    @endphp
                    <option value="{{$row->id}}" {{$selected}}>{{$row->name}}</option>
                    @endforeach
                  </select>
                </div>
              </div>

              <div class="form-group text-center">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                  <button type="submit" class="btn btn-primary">Sign Up Now</button>
                </div>
              </div>

            </form>
          </div>

        </div>
      </div>
    </div>
  </div>
  <script>
  $(function() {

    function checkPasswordMatch(){
      var password = $('#password').val();
      var password_confirmation = $('#password_confirmation').val();

      if(password != '' && password_confirmation != ''){
        if(password != password_confirmation){
          $('#divCheckPasswordMatch').html("Password doesn't match.");
        }
        else {
          $('#divCheckPasswordMatch').html("");
        }
      }
      else {
        $('#divCheckPasswordMatch').html("");
      }
    }

    $("#password, #password_confirmation").on('keyup', function(e){
      //console.log(e);
      checkPasswordMatch();
    });

  });
  </script>
</body>
</html>
