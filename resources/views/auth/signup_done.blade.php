<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Apps | Register</title>

  <!-- Bootstrap -->
  <link href="{{ asset('vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
  <!-- Font Awesome -->
  <link href="{{ asset('vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
  <!-- NProgress -->
  <link href="{{ asset('vendors/nprogress/nprogress.css') }}" rel="stylesheet">
  <!-- Animate.css -->
  <link href="{{ asset('vendors/animate.css/animate.min.css') }}" rel="stylesheet">
  <!-- Custom Theme Style -->
  <link href="{{ asset('build/css/custom.min.css') }}" rel="stylesheet">
  <!-- jQuery -->
  <script src="{{ asset('/vendors/jquery/dist/jquery.min.js') }}"></script>
</head>

<body class="login">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
          <div class="panel-heading"><h4 style="color:#73879c">Sign Up</h4></div>

          <div class="panel-body">
            <div class="text-center">
              We've Sent You a Verification Email.<br>
              Time to Check Your Email.<br>
              <p>Click the link in your email to verify your account.</p>
              or
              <p>
                <a href="/login" style='color:#337ab7;text-decoration: underline;'>Click here</a> to login.
              </p>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</body>
</html>
