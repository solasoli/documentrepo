@extends('layouts.app')

@section('content')

  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>User</h3>
      </div>
    </div>

    <div class="clearfix"></div>
    @if(Session::has('message'))
      <p class="alert alert-success">{{ Session::get('message') }}</p>
    @endif

    @if(Session::has('error'))
      <p class="alert alert-danger">{{ Session::get('error') }}</p>
    @endif
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Filter</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <form class="form-horizontal form-label-left form-filter" >
              <div class="form-group">
                <div class='control-label col-md-3 col-sm-3 col-xs-12'>Status :</div>
                <div class='col-md-6'>
                  <select name="filter" class="form-control" required="required">
                    <option value="show_all">Show all</option>
                    <option value="0">Active</option>
                    <option value="1">Suspended</option>
                  </select>
                </div>
              </div>
              <!-- <div class="form-group">
                <div class='control-label col-md-3 col-sm-3 col-xs-12'></div>
                <div class='col-md-6'>
                  <button type="submit" class="btn btn-info">Cari</button>
                </div>
              </div> -->
            </form>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>User<small id="sub_xtitle"></small></h2>
            <div class="pull-right">
              @if(can_access('user', 'add'))
              <a class='btn btn-sm btn-success' href='{{url()->current()}}/add'><i class='fa fa-plus'></i> Add</a>
              @endif
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <table class="table table-bordered" id="users-table" width="100%">
               <thead>
                   <tr>
                       <th class="hide">is_suspended</th>
                       <th>Username</th>
                       <th>Email</th>
                       <th>Title</th>
                       <th>Affiliation</th>
                       <th>Organization</th>
                       <th>Country of residence</th>
                       <th>Role</th>
                       <th>Status</th>
                       <th style='width:100px'></th>
                   </tr>
               </thead>
           </table>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection

@section('scripts')
<!-- DataTables -->
<script src="{{ asset('/vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-scroller/js/dataTables.scroller.min.js') }}"></script>
<script>
$(function() {


  var oTable = $('#users-table').DataTable({
    processing: true,
    serverSide: true,
    ajax: '{{url()->current()}}/datatables/',
    columnDefs: [{
      targets: 9, // action button
      className: 'text-center'
    }],
    columns: [
      { data: 'is_suspended', name: 'is_suspended', visible: false },
      { data: 'username', name: 'username' },
      { data: 'email', name: 'email' },
      { data: 'title', render: function(data,type,row){
        return data != null ? data : '-';
      } },
      { data: 'affiliation', render: function(data,type,row){
        return data != null ? data : '-';
      } },
      { data: 'organization', render: function(data,type,row){
        return data != null ? data : '-';
      } },
      { data: 'country', render: function(data,type,row){
        return data != null ? data : '-';
      } },
      { data: 'role', name: 'role' },
      { data: 'is_suspended', render: function(data,type,row){
        return data == 0 ? 'Active' : 'Suspended';
      } },
      { data: null, render: function ( data, type, row ) {
          var return_button = "";

          @if(can_access('user', 'edit'))
          return_button += "<a class='btn btn-warning btn-xs' href='{{url()->current()}}/edit/" + data.guid + "' title='Edit'><i class='fa fa-pencil'></i></a>";
          @endif

          @if(can_access('user', 'suspend'))
          return_button += "<a class='btn btn-danger btn-xs' href='{{url()->current()}}/delete/" + data.guid + "' title='Suspend'><i class='fa fa-close'></i></a>";
          @endif

          @if(can_access('user', 'delete'))
          return_button += "<a class='btn btn-danger btn-xs' href='{{url()->current()}}/delete/" + data.guid + "' title='Delete'><i class='fa fa-trash'></i></a>";
          @endif

          return return_button;
        }},
    ]
  });

  /* Filter */
  $('select[name="filter"]').on("change", function(e){
    var filter = $(this).val();
    var xtitle = '';

    if(filter > -1){
      xtitle += $(this).find('option:selected').text();

      oTable
        .columns(0)
        .search(filter)
        .draw();
    }
    else { //Tampilkan Semua
      oTable
        .columns(0)
        .search('1|0', true, false)
        .draw();
    }

      $("#sub_xtitle").html(xtitle);
  });
});
</script>
@endsection
