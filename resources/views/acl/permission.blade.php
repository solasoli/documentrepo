@extends('layouts.app')

@section('content')
    <div class="page-title">
      <div class="title_left">
        <h3>Permission</h3>
      </div>
    </div>

    <div class="clearfix"></div>
    @if(Session::has('message'))
      <p class="alert alert-success">{!! Session::get('message') !!}</p>
    @endif

  <form class="form-horizontal form-label-left" method="POST" >
  {{ csrf_field() }}
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Role</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
              <div class="form-group">
                <div class="control-label col-md-3 col-sm-3 col-xs-12">Role</div>
                <div class="col-md-6">
                  <select name="role" class="form-control" required="required">
                    @foreach($role as $idx => $row)
                    <option value="{{$row->id}}" {{$row->id == $current_role ? "selected" : ""}}>{{$row->nama}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <!-- <div class="form-group">
                <div class="control-label col-md-3 col-sm-3 col-xs-12"></div>
                <div class="col-md-6">
                  <button type="submit" class="btn btn-info">Lihat</button>
                </div>
              </div> -->
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Permission</h2>
            <div class="pull-right">
              <button type="submit" class="btn btn-warning"><i class="fa fa-refresh"></i> Update Permission</button>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <table class="table table-bordered table-striped" id="users-table" style="width:100%">
              <tr>
                <th style="width: 1px;white-space: nowrap;">#</th>
                <th>Menu</th>
                <th style="width: 1px;white-space: nowrap;">View</th>
                <th style="width: 1px;white-space: nowrap;">Add</th>
                <th style="width: 1px;white-space: nowrap;">Edit</th>
                <th style="width: 1px;white-space: nowrap;">Delete</th>
                <th style="width: 1px;white-space: nowrap;">Approve</th>
                <th style="width: 1px;white-space: nowrap;">Reject</th>
                <th style="width: 1px;white-space: nowrap;">Suspend</th>
              </tr>
              @foreach($menu as $idx => $row)
              @php
              $view_checked = isset($data_array['view'][$row->id]) && $data_array['view'][$row->id] == 1 ? "checked" : "";
              $add_checked = isset($data_array['add'][$row->id]) && $data_array['add'][$row->id] == 1 ? "checked" : "";
              $edit_checked = isset($data_array['edit'][$row->id]) && $data_array['edit'][$row->id] == 1 ? "checked" : "";
              $delete_checked = isset($data_array['delete'][$row->id]) && $data_array['delete'][$row->id] == 1 ? "checked" : "";
              $approve_checked = isset($data_array['approve'][$row->id]) && $data_array['approve'][$row->id] == 1 ? "checked" : "";
              $reject_checked = isset($data_array['reject'][$row->id]) && $data_array['reject'][$row->id] == 1 ? "checked" : "";
              $suspend_checked = isset($data_array['suspend'][$row->id]) && $data_array['suspend'][$row->id] == 1 ? "checked" : "";
              @endphp
              <tr>
                <td>{{$row->id}}</td>
                <td>{{$row->nama}} <input type="hidden" name="menu[]" value={{$row->id}}></td>
                <td align="center"><input name="view[{{$row->id}}]" type="checkbox" value="1" {{$view_checked}}></td>
                <td align="center"><input name="add[{{$row->id}}]" type="checkbox" value="1" {{$add_checked}}></td>
                <td align="center"><input name="edit[{{$row->id}}]" type="checkbox" value="1" {{$edit_checked}}></td>
                <td align="center"><input name="delete[{{$row->id}}]" type="checkbox" value="1" {{$delete_checked}}></td>
                <td align="center"><input name="approve[{{$row->id}}]" type="checkbox" value="1" {{$approve_checked}}></td>
                <td align="center"><input name="reject[{{$row->id}}]" type="checkbox" value="1" {{$reject_checked}}></td>
                <td align="center"><input name="suspend[{{$row->id}}]" type="checkbox" value="1" {{$suspend_checked}}></td>
              </tr>
              @endforeach
            </table>
          </div>
        </div>
      </div>
    </div>
  </form>
@endsection

@section('scripts')
<!-- DataTables -->
<script src="{{ asset('/vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-scroller/js/dataTables.scroller.min.js') }}"></script>
<script>

$(function(){

  setTimeout(function() {
    $(".alert-success").hide(1000);
  }, 3000);

  $("select[name='role']").on("change", function(){
    location.href = '/acl/permission/' + $(this).val();
    return false;
  });

});
</script>
@endsection
