@extends('layouts.app')

@section('content')
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Submission</h3>
      </div>
    </div>

    <div class="clearfix"></div>
    @if(Session::has('message'))
      <p class="alert alert-success">{!! Session::get('message') !!}</p>
    @endif

    @if(Session::has('error'))
      <p class="alert alert-danger">{!! Session::get('error') !!}</p>
    @endif

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Filter</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <form class="form-horizontal form-label-left form-filter" >
              <div class="form-group">
                <div class='control-label col-md-3 col-sm-3 col-xs-12'>Status :</div>
                <div class='col-md-6'>
                  <select name="filter" class="form-control" required="required">
                    <option value="0">Show all</option>
                    @foreach($submission_status AS $row)
                    <option value="{{$row->id}}">{{$row->name}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <!-- <div class="form-group">
                <div class='control-label col-md-3 col-sm-3 col-xs-12'></div>
                <div class='col-md-6'>
                  <button type="submit" class="btn btn-info">Cari</button>
                </div>
              </div> -->
            </form>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Submission<small id="sub_xtitle"></small></h2>
            @if(can_access('submission', 'add'))
            <div class="pull-right">
                <a class='btn btn-sm btn-success' href='{{url()->current()}}/add'><i class='fa fa-plus'></i> Add</a>
            </div>
            @endif
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <table class="table table-bordered" id="users-table" width="100%">
             <thead>
               <tr>
                 <th class="hide">status_id</th>
                 <th>Title</th>
                 <th>Sub title</th>
                 <th>Author</th>
                 <th>Publication type</th>
                 <th>Abstract</th>
                 <th>Research site</th>
                 <th>Source link</th>
                 <th>Soft copy</th>
                 <th>Status</th>
                 <th style='width:100px'></th>
               </tr>
             </thead>
           </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
<!-- DataTables -->
<script src="{{ asset('/vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-scroller/js/dataTables.scroller.min.js') }}"></script>
<script>
$(function() {
  setTimeout(function() {
    $(".alert-success").hide(1000);
  }, 3000);

  var oTable = $('#users-table').DataTable({
      processing: true,
      serverSide: true,
      ajax: '{{url()->current()}}/datatables/',
      columnDefs: [{
        targets: 10, // action button
        className: 'text-center'
      }],
      columns: [
        { data: 'status_id', name: 'status_id', visible: false },
        { data: 'title', name: 'title' },
        { data: 'sub_title', name: 'sub_title' },
        { data: null, render: function(data,type,row){
          var first_letter_of_firstname = data.author_firstname.toUpperCase().charAt(0);
          return data.author_lastname + ', ' + first_letter_of_firstname + ' ' + data.author_middlename
        }},
        { data: 'publication_type', name: 'publication_type' },
        { data: 'abstract', name: 'abstract' },
        { data: 'research_site_country', render: function(data,type,row){
          return row.country + ', ' + row.research_site_region;
        } },
        { data:null, orderable: false, render: function(data,type,row){
          return data.source_link == null || data.source_link == '' ? '-' : data.source_link;
        } },
        { data: null, orderable: false, render: function(data,type,row){
          var text = '';
          if(data.soft_copy == null || data.soft_copy == ''){
            text += '-';
          } else {
            text += "<a style='color:#337ab7;text-decoration: underline;' href='{{url()->current()}}/dl/" + data.guid + "'  title='Download'>Download</a><br>";
            text += data.download_option_id == 1 ? '(public can download this file)' : '';
          }
          return text;
        } },
        { data: 'status', name: 'status' },
        { data: null, orderable: false, render: function ( data, type, row ) {
          var return_button = '';

          @if(can_access('submission', 'edit'))
          return_button += "<a class='btn btn-warning btn-xs' href='{{url()->current()}}/edit/" + data.guid + "'  title='Edit'><i class='fa fa-pencil'></i></a>";
          @endif

          @if(can_access('submission', 'delete'))
          return_button += "<a class='btn btn-danger btn-xs' href='{{url()->current()}}/delete/" + data.guid + "'  title='Delete' onclick='return confirm(\"Are you sure want to delete this data?\")'><i class='fa fa-trash'></i></a>";
          @endif

          if(data.status_id == 1){ //
            @if(can_access('submission', 'approve'))
            return_button += "<a class='btn btn-primary btn-xs' href='{{url()->current()}}/update_submission_status/2/" + data.guid + "'  title='Approve' onclick='return confirm(\"Approve this data?\")'><i class='fa fa-check'></i></a>";
            @endif

            @if(can_access('submission', 'reject'))
            return_button += "<a class='btn btn-danger btn-xs' href='{{url()->current()}}/update_submission_status/3/" + data.guid + "'  title='Reject' onclick='return confirm(\"Reject data data?\")'><i class='fa fa-close'></i></a>";
            @endif
          }
          return return_button;
        }},
      ]
  });

  /* Filter */
  $('select[name="filter"]').on("change", function(e){
    var filter = $(this).val();
    var xtitle = '';

    if(filter > 0){
      xtitle += $(this).find('option:selected').text();

      oTable
        .columns(0)
        .search(filter)
        .draw();
    }
    else { //Tampilkan Semua
      oTable
        .columns(0)
        .search('1|2|3', true, false)
        .draw();
    }

      $("#sub_xtitle").html(xtitle);
  });
});
</script>
@endsection
