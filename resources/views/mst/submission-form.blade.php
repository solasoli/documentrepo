@extends('layouts.app')

@section('content')

<div class="">
  <div class="page-title">
    <div class="title_left">
      <h3>Submission</h3>
    </div>
  </div>

  <div class="clearfix"></div>
  @if ($errors->any())
    <div class="alert alert-danger">
      <ul class="list-unstyled">
        @foreach ($errors->all() as $error)
          <li>&#8226; {{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif
  @if(Session::has('message'))
    <p class="alert alert-success">{!! Session::get('message') !!}</p>
  @endif
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Submission Form</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <form class="form-horizontal" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" >
                Title <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input name='title' value='{{ !is_null(old('title')) ? old('title') : (isset($data->title) ? $data->title : '') }}'  class="form-control col-md-7 col-xs-12" type="text">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" >
                Sub-title <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input name='sub_title' value='{{ !is_null(old('sub_title')) ? old('sub_title') : (isset($data->sub_title) ? $data->sub_title : '') }}'  class="form-control col-md-7 col-xs-12" type="text">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" >
                Author first name<span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input name='author_firstname' value='{{ !is_null(old('author_firstname')) ? old('author_firstname') : (isset($data->author_firstname) ? $data->author_firstname : '') }}'  class="form-control col-md-7 col-xs-12" type="text">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" >
                Author middle name<span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input name='author_middlename' value='{{ !is_null(old('author_middlename')) ? old('author_middlename') : (isset($data->author_middlename) ? $data->author_middlename : '') }}'  class="form-control col-md-7 col-xs-12" type="text">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" >
                Author last name<span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input name='author_lastname' value='{{ !is_null(old('author_lastname')) ? old('author_lastname') : (isset($data->author_lastname) ? $data->author_lastname : '') }}'  class="form-control col-md-7 col-xs-12" type="text">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" >
                Publication type<span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <select class="form-control" name="publication_type">
                  @foreach($publication_type AS $row)
                  @php
                  $selected = !is_null(old('publication_type')) ? old('publication_type') : (isset($data) ? $data->publication_type_id : '');
                  $selected = $selected == $row->id ? "selected" : "";
                  @endphp

                  <option value="{{$row->id}}" {{$selected}}>{{$row->name}}</option>
                  @endforeach
                </select>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" >
                Abstract<span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input name='abstract' value='{{ !is_null(old('abstract')) ? old('abstract') : (isset($data->abstract) ? $data->abstract : '') }}'  class="form-control col-md-7 col-xs-12" type="text">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" >
                Research site country<span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <select class="form-control" name="research_site_country">
                  @foreach($country AS $row)
                  @php
                  $selected = !is_null(old('publication_type')) ? old('publication_type') : (isset($data->research_site_country) ? $data->research_site_country : '');
                  $selected = $selected == $row->id ? "selected" : "";
                  @endphp
                  <option value="{{$row->id}}" {{$selected}}>{{$row->name}}</option>
                  @endforeach
                </select>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" >
                Research site region<span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input name='research_site_region' value='{{ !is_null(old('research_site_region')) ? old('research_site_region') : (isset($data->research_site_region) ? $data->research_site_region : '') }}'  class="form-control col-md-7 col-xs-12" type="text">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" >
                Source link<span class="required"></span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input name='source_link' value='{{ !is_null(old('source_link')) ? old('source_link') : (isset($data->source_link) ? $data->source_link : '') }}'  class="form-control col-md-7 col-xs-12" type="text">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" >
                Soft copy <span class="required"></span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input name='soft_copy' value='{{ !is_null(old('soft_copy')) ? old('soft_copy') : (isset($data->soft_copy) ? $data->soft_copy : '') }}' class="form-control col-md-7 col-xs-12" type="file" accept="application/pdf">
                @if(isset($data->soft_copy))
                <div class="text-warning">Ignore if you don't want to change the soft copy</div>
                @endif
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" >
                Copyright clearance<span class="required"></span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <table border="0">
                  @foreach($download_option AS $row)
                  @php
                  $checked = !is_null(old('copyright_clearance')) ? old('copyright_clearance') : (isset($data->download_option_id) ? $data->download_option_id : '');
                  $checked = $checked == $row->id ? "checked" : "";
                  @endphp
                  <tr>
                    <td><input type="radio" name="copyright_clearance" value="{{$row->id}}" {{$checked}}> {{$row->name}}</td>
                  </tr>
                  @endforeach
                </table>
              </div>
            </div>

            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                <a href='{{url('')}}/mst/submission' class="btn btn-primary" type="button">Cancel</a>
                <button type="submit" class="btn btn-success">Submit</button>
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(function() {
  setTimeout(function() {
    $(".alert-success").hide(1000);
  }, 3000);
});
</script>
@endsection
